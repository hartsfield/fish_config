set fish_color_cwd #3F9BBF
#set -x LD_LIBRARY_PATH {LD_LIBRARY_PATH}:/usr/local/lib
set EDITOR /usr/bin/vim
set TERM xterm-256color
set -x GOPATH /home/zxcv/Documents/go/
set -x GOBIN /home/zxcv/Documents/go/bin
set -x GOSRC /home/zxcv/Documents/go/src/gitlab.com/hartsfield/
set PATH /home/zxcv/Documents/go/bin $PATH
set PATH /home/zxcv/Documents/bin/ $PATH
set PATH /usr/local/go/bin $PATH
set -x GOROOT /usr/local/go
set fish_color_command green bold
set fish_color_param purple

autojump > /dev/null
